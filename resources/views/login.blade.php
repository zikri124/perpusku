<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Perpusku - Login</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css">
</head>

<body>
    <div class="card card bg shadow">
        <h1 class="display-3">Login</h1>
        <form action="/auth/login" method="post" class="form">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="form-floating mb-3">
                <input type="email" name="email" class="form-control" id="floatingInput" placeholder="Email">
                <label for="floatingInput">Email</label>
            </div>
            <div class="form-floating mb-3">
                <input type="password" name="password" class="form-control" id="floatingInput3" placeholder="Password">
                <label for="floatingInput3">Password</label>
            </div>
            <button type="submit" class="btn btn-primary center mb-3">Login</button>
            <br>
            <p class="center">Didn't have an account?</p>
            <a href="/auth/register">
                <button type="button" class="btn btn-outline-secondary center mb-3">Register</button>
            </a>
        </form>
    </div>
</body>

</html>
