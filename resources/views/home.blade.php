<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PerpusKu - Home</title>
</head>

<body>
    @extends('template')

    @section('content')
        <h1 class="display-3">PerpusKu</h1>
        <br>
        <form action="/search" method="get" class="form">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Cari Judul Buku" aria-label="Cari Judul Buku"
                    aria-describedby="button-addon2" name="searchWord">
                <button class="btn btn-outline-success" type="submit" id="button-addon2">Cari</button>
            </div>
        </form>
        <h1 class="display-6">{{ $message }}</h1>
        @if ($found)
            @foreach ($data->unique('tittle') as $items)
                <div class="card buku">
                    <div class="row">
                        <div class="col">
                            <div class="bookphoto">
                                <img src="{{url('/images/booktemp.jpg')}}" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-9">
                            <p>Judul : {{ $items['tittle'] }}</p>
                            <p>Penulis : {{ $items['writer'] }}</p>
                            <p>Penerbit : {{ $items['publisher'] }}</p>
                            <a href="/books/{{ $items['tittle'] }}"><button class="btn btn-outline-primary" type="submit" id="button-addon2">Lihat</button></a>
                        </div>
                    </div>
                </div>
                <br>
            @endforeach            
        @endif
    @endsection
</body>

</html>
