<!DOCTYPE html>
<html lang="en">

<head>
    <title>PERPUSKU</title>
</head>

<body>
    @extends('template')

    @section('content')
        <h1 class="display-3">Buku Pinjaman Kamu</h1>
        <br>
        <table class="table">
            <thead class="table-dark">
                <th>No</th>
                <th>No Buku</th>
                <th>Judul Buku</th>
                <th>Penulis</th>
                <th>Penerbit</th>
                <th>Tanggal Pinjam</th>
                <th>Tanggal Kembali</th>
            </thead>
            <tbody>
                @php
                    $i = 0;
                @endphp
                @foreach ($items as $data)
                    <tr>
                        <td>{{ $i += 1 }}</td>
                        <td>{{ $data['book_id'] }}</td>
                        <td>{{ $data['tittle'] }}</td>
                        <td>{{ $data['writer'] }}</td>
                        <td>{{ $data['publisher'] }}</td>
                        <td>{{ $data['borrow_date'] }}</td>
                        <td>{{ $data['return_date'] }}</td>
                    </tr>
                @endforeach

                {{-- @foreach ($data as $items)
                    <tr>
                        <td>{{ $i += 1 }}</td>
                        @foreach ($items as $item)
                            <td>{{ $item }}</td>
                        @endforeach

                    </tr>
                @endforeach --}}
            </tbody>
        </table>
    @endsection
</body>

</html>
