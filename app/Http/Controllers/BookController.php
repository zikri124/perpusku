<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{
    public function getBooks()
    {
        $books = Book::all();


        if ($books && count($books) > 0) {
            return view('home', ['found' => true, 'data' => $books, 'message' => ""]);
        } else {
            return view('home', ['found' => false, 'message' => "Buku tidak ditemukan"]);
        }
    }

    public function getBook($tittle)
    {
        $books = Book::where('tittle', $tittle)->get();

        if ($books && count($books) > 0) {
            return view('search', ['found' => true, 'data' => $books]);
        } else {
            return view('search', ['found' => false, 'message' => "Buku tidak ditemukan"]);
        }
    }

    public function getbySearchBook(Request $request)
    {
        $searchWord = $request->searchWord;

        $books = Book::query()
            ->where('tittle', 'LIKE', $searchWord . '%')
            ->get();

        if ($books && count($books) > 0) {
            return view('home', ['found' => true, 'data' => $books, 'message' => 'Hasil pencarian untuk "'.$searchWord.'"']);
        } else {
            return view('home', ['found' => false, 'message' => 'Buku yang kamu cari tidak ditemukan']);
        }
    }
}
