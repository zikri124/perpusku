<?php

namespace App\Http\Controllers;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getAllUsers(Request $request)
    {
        $users = User::all();
        return response()->json([
            'success' => true,
            'message' => 'Success get all users',
            'data' => [
                'users' => $users
            ]
        ], 200);
    }

    public function showOneUser(Request $request, $userId)
    {
        $token = $request->bearerToken();
        $jwt = JWT::decode($token, env('JWT_KEY'), ['HS256']);
        $email = $jwt->sub;
        $role = $jwt->role;

        $user = $role == 'admin' ? User::find($userId)
            : User::where('email', $email)->first();

        if ($user) {
            if ($user->role != 'user') {
                return response()->json([
                    'success' => true,
                    'message' => 'Successfully get user',
                    'data' => ['user' => $user]
                ], 200);
            } else if ($user->id != $userId) {
                return response()->json([
                    'success' => false,
                    'message' => 'Forbidden'
                ], 403);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => 'Successfully get user',
                    'data' => ['user' => $user]
                ], 200);
            }
        } else {
            return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
        }
    }
}
