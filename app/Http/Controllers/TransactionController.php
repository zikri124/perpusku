<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Transaction;
use App\Models\Book;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function showTransactions(Request $request)
    {
        $userId = Auth::id();

        $user = User::where('id', $userId)->first();
        $role = $user->role;

        $transactions = Transaction::where('user_id', $user->id)->get();

        if ($transactions) {
            foreach ($transactions as $transaction) {
                $data = Book::find($transaction->book_id);
                // $bookArray = array(
                //     'title' => $data->title,
                //     'writer' => $data->writer
                // );
                // $book = (object) $bookArray;
                $transaction->tittle = $data->tittle;
                $transaction->writer = $data->writer;
                $transaction->publisher = $data->publisher;
            }
            return view('usertransaction', ['items' => $transactions]);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Forbidden'
            ], 403);
        }
    }

    public function getTransactionbyid(Request $request, $transactionId)
    {
        $token = $request->bearerToken();
        $jwt = JWT::decode($token, env('JWT_KEY'), ['HS256']);
        $email = $jwt->sub;
        $user = User::where('email', $email)->first();
        $role = $jwt->role;
        $transaction = Transaction::find($transactionId);

        if (!$transaction) {
            return response()->json([
                'success' => false,
                'message' => 'Transaksi Not Found'
            ], 404);
        }

        if ($role == 'admin' || $user->id == $transaction->user_id) {
            $book = Book::find($transaction->book_id);
            $user2 = User::find($transaction->user_id);

            return response()->json([
                'success' => true,
                'message' => 'Transaksi Ditemukan',
                'data' => ['transaction' => [
                    'user' => [
                        'name' => $user2->name,
                        'email' => $user2->email,
                    ],
                    'book' => [
                        'title' => $book->title,
                        'author' => $book->author,
                        'description' => $book->description,
                        'synopsis' => $book->synopsis,
                    ],
                    'deadline' => $transaction->deadline,
                    'created_at' => $transaction->created_at,
                    'updated_at' => $transaction->updated_at,
                ]]
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Transaksi Forbidden'
            ], 403);
        }
    }

    public function editTransaction(Request $request, $transactionId)
    {
        $token = $request->bearerToken();
        $jwt = JWT::decode($token, env('JWT_KEY'), ['HS256']);
        $email = $jwt->sub;
        $user = User::where('email', $email)->first();
        $role = $jwt->role;
        $transaction = Transaction::find($transactionId);

        if (!$transaction) {
            return response()->json([
                'success' => false,
                'message' => 'Transaksi Not Found'
            ], 404);
        }

        if ($role == 'admin') {
            $book = Book::find($transaction->book_id);
            $user2 = User::find($transaction->user_id);
            $transaction->deadline = $request->deadline;
            $transaction->save();
            return response()->json([
                'success' => true,
                'message' => 'Transaksi Berhasil Diubah',
                'data' => ['transaction' => [
                    'user' => [
                        'name' => $user2->name,
                        'email' => $user2->email,
                    ],
                    'book' => [
                        'title' => $book->title,
                        'author' => $book->author,
                        'description' => $book->description,
                        'synopsis' => $book->synopsis,
                    ],
                    'deadline' => $transaction->deadline,
                    'created_at' => $transaction->created_at,
                    'updated_at' => $transaction->updated_at,
                ]]
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Transaksi Forbidden'
            ], 403);
        }
    }
}
