<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $emailExist = User::select('*')
            ->where('email', $request->email)
            ->get();

        if ($validation->fails()) {
            return redirect('/auth/register')->with('status', 'Your input is invalid!');
        }

        if (sizeof($emailExist) == 0) {
            $hashed = Hash::make($request->password);

            User::create(['name' => $request->name, 'email' => $request->email, 'password' => $hashed]);

            Auth::attempt(['email' => $request->email, 'password' => $request->password]);

            return redirect('/');
        } else {
            return redirect('/auth/register')->with('status', 'Your email already registered');
        }
    }

    public function login(Request $request)
    {
        $validation = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (!$validation) {
            return redirect('/auth/login')->with('status', 'Your input is invalid!');
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('/');
        } else {
            return redirect('/auth/login')->with('status', 'User not found or wrong password');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect('/auth/login');
    }
}
