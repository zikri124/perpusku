<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\TransactionController;
use App\Models\Transaction;

Route::group(['prefix' => 'auth'], function () {
    Route::get('/login', function () {
        return view('login');
    });
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/register', function () {
        return view('register');
    });
    Route::post('/register', [AuthController::class, 'register']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::group(['middleware' => 'check.session'], function () {
    Route::get('/', [BookController::class, 'getBooks']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('/', [UserController::class, 'getUser']);
    });

    Route::group(['prefix' => 'transactions'], function () {
        Route::get('/mine', [TransactionController::class, 'showTransactions']);
        Route::put('/{transactionId}', [TransactionController::class, 'editTransaction']);
    });

    Route::group(['prefix' => 'books'], function () {

    });
});

Route::group(['prefix' => 'books'], function () {
    Route::get('/', [
        BookController::class, 'getBooks'
    ]);

    Route::get('/{tittle}', [
        BookController::class, 'getBook'
    ]);
});

Route::get('/search', [
    BookController::class, 'getbySearchBook']);
